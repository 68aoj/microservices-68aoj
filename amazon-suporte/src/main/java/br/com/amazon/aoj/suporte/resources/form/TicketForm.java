package br.com.amazon.aoj.suporte.resources.form;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class TicketForm {
	
	private final String descricao;
	private final String email;
}
