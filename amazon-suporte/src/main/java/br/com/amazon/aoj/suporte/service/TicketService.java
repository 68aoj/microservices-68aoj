package br.com.amazon.aoj.suporte.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.amazon.aoj.suporte.domain.Ticket;
import br.com.amazon.aoj.suporte.repository.TicketRepository;

@Service
public class TicketService {
	
	@Autowired
	private TicketRepository repository;

	public Optional<Ticket> obter(long id) {
		return repository.findById(id);
	}

	@Transactional
	public Ticket criar(String descricao, String email) {
		final Ticket ticket = Ticket.builder().descricao(descricao).email(email).build();
		return repository.save(ticket);
	}

	public List<Ticket> listarPorEmail(String email) {
		return repository.findByEmail(email);
	}

}
