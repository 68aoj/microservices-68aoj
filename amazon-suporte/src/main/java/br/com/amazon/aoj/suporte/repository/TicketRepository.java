package br.com.amazon.aoj.suporte.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.amazon.aoj.suporte.domain.Ticket;

public interface TicketRepository extends JpaRepository<Ticket, Long>{
	
	List<Ticket> findByEmail(String email);

}
