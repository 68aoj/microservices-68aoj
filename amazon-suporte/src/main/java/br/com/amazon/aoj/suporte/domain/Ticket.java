package br.com.amazon.aoj.suporte.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor(force=true)
@Entity
@Table(name="Ticket")
public class Ticket {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private final Long id = null;
	
	private String descricao;
	private String email;
	
	@Builder.Default
	@Enumerated(EnumType.STRING)
	private StatusTicket statusTicket = StatusTicket.ABERTO;

	public static enum StatusTicket {
		ABERTO, EM_ANALISE, FECHADO;
	}
	

}
