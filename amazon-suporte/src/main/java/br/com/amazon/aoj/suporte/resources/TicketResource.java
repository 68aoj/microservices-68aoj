package br.com.amazon.aoj.suporte.resources;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.amazon.aoj.suporte.domain.Ticket;
import br.com.amazon.aoj.suporte.resources.form.TicketForm;
import br.com.amazon.aoj.suporte.service.TicketService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping("tickets")
@Api(tags="Tickets", description="Serviços de abertura e acompanhamento de Tickets")
public class TicketResource {

	@Autowired
	private TicketService service;
	
	@PostMapping("/")
	@ApiOperation(value = "Criação de Tickets",  consumes = APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> abrirTicket(@RequestBody TicketForm form, final HttpServletRequest request) {
		UriComponentsBuilder location = UriComponentsBuilder
				.fromPath(request.getRequestURL().append("/{id}").toString());
		URI uri = location.buildAndExpand(service.criar(form.getDescricao(), form.getEmail()).getId()).toUri();
		return ResponseEntity.created(uri).build();
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Buscar Ticket por id",  consumes = APPLICATION_JSON_VALUE, response=Ticket.class, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<Ticket> buscar(@PathVariable Long id) {
		final Optional<Ticket> ticket = service.obter(id);
		return ticket.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
	}
	
	@GetMapping("/email/{email}")
	@ApiOperation(value = "Buscar Tickets por email",  consumes = APPLICATION_JSON_VALUE, response=Ticket.class, responseContainer="List", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Ticket>> listarChamadosPorEmail(@PathVariable String email) {
		final List<Ticket> tickets = service.listarPorEmail(email);
		if(tickets.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(tickets);
	}
	
}
