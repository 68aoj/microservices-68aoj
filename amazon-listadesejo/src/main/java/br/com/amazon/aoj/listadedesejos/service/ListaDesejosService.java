package br.com.amazon.aoj.listadedesejos.service;

import static java.util.stream.Collectors.toSet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.amazon.aoj.listadedesejos.domain.ListaDesejos;
import br.com.amazon.aoj.listadedesejos.domain.ProdutoListaDesejo;
import br.com.amazon.aoj.listadedesejos.repository.ListaDesejosRepository;
import br.com.amazon.aoj.listadedesejos.resources.form.ProdutoListaDesejoForm;

@Service
public class ListaDesejosService {

	@Autowired
	private ListaDesejosRepository repository;
	
	@Transactional(rollbackFor = Exception.class)
	public ListaDesejos criarListaDesejos(String nome, String email, List<ProdutoListaDesejoForm> produtos) {
		final ListaDesejos listaDesejo = ListaDesejos.builder()
				.email(email)
				.nome(nome)
				.produtos(produtos.stream().map(produto ->
					ProdutoListaDesejo.builder()
						.categoria(produto.getCategoria())
						.fabricante(produto.getFabricante())
						.id(produto.getId())
						.nome(produto.getNome())
					.build()
				).collect(toSet())).build();
		
		return repository.save(listaDesejo);	
	}
	
	public ListaDesejos obter(Long id) {
		return repository.findById(id).get();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public ListaDesejos adicionarProduto(Long idListaDeDesejos, ProdutoListaDesejo produto) {
		final ListaDesejos listaDesejos = obter(idListaDeDesejos);
		listaDesejos.adicionarProduto(produto);
		repository.save(listaDesejos);
		return listaDesejos;
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void alterarNomeDoProdutoNosPedidos(Long produtoId, String nome) {
		List<ListaDesejos> listaDeDesejos = repository.findAll();
		listaDeDesejos.stream()
				.flatMap(p -> p.getProdutos().stream())
				.filter(produto -> produto.getId().equals(produtoId))
				.forEach(produto -> produto.setNome(nome));

		repository.saveAll(listaDeDesejos);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void removerProdutoDeletado(Long produtoId) {
		List<ListaDesejos> listaDeDesejos = repository.findAll();
		listaDeDesejos.forEach(produto -> produto.removerProduto(produtoId));
		repository.saveAll(listaDeDesejos);
	}


	public List<ListaDesejos> obterPorNomeEmail(String nome, String email) {
		return repository.findAllByNomeAndEmail(nome, email);
	}

	public List<ListaDesejos> listarPorEmail(String email) {
		return repository.findAllByEmail(email);
		
	}

	public ListaDesejos obterPorId(Long id) {
		return repository.findById(id).get();
	}
}
