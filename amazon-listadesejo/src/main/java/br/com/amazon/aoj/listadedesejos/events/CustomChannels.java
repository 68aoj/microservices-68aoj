package br.com.amazon.aoj.listadedesejos.events;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface CustomChannels {
	
	@Input("inboundProdutoChanges")
	SubscribableChannel orgs();
}