package br.com.amazon.aoj.listadedesejos.events.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

import br.com.amazon.aoj.listadedesejos.events.CustomChannels;
import br.com.amazon.aoj.listadedesejos.events.models.ProdutoChangeModel;
import br.com.amazon.aoj.listadedesejos.service.ListaDesejosService;

@EnableBinding(CustomChannels.class)
public class ProdutoChangeHandler {
	
	@Autowired
	private ListaDesejosService listaDesejosService; 

	private static final Logger logger = LoggerFactory.getLogger(ProdutoChangeHandler.class);

	@StreamListener("inboundProdutoChanges")
	public void loggerSink(ProdutoChangeModel produtoChange) {
		logger.debug("Received a message of type " + produtoChange.getType());
		switch (produtoChange.getAction()) {
		case "UPDATE":
			logger.debug("Received a UPDATE event from the produto service for produto id {}",
					produtoChange.getProdutoId());
			listaDesejosService.alterarNomeDoProdutoNosPedidos(produtoChange.getProdutoId(), produtoChange.getNome());
			break;
		case "DELETE":
			logger.debug("Received a DELETE event from the produto service for produto id {}",
					produtoChange.getProdutoId());
			listaDesejosService.removerProdutoDeletado(produtoChange.getProdutoId());
			break;
		default:
			logger.error("Received an UNKNOWN event from the produto service of type {}", produtoChange.getType());
			break;

		}
	}

}
