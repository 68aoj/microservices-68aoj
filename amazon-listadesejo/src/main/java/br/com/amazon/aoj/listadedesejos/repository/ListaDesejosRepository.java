package br.com.amazon.aoj.listadedesejos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.amazon.aoj.listadedesejos.domain.ListaDesejos;


public interface ListaDesejosRepository extends JpaRepository<ListaDesejos, Long>{

	List<ListaDesejos> findAllByNomeAndEmail(String nome, String email) ;

	List<ListaDesejos> findAllByEmail(String email);

	List<ListaDesejos> findByEmail(String email);

}
