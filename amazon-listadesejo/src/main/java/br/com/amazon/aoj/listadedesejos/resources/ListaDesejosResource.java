package br.com.amazon.aoj.listadedesejos.resources;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.amazon.aoj.listadedesejos.domain.ListaDesejos;
import br.com.amazon.aoj.listadedesejos.domain.ProdutoListaDesejo;
import br.com.amazon.aoj.listadedesejos.resources.form.ListaDesejosForm;
import br.com.amazon.aoj.listadedesejos.service.ListaDesejosService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "listadesejos")
@Api(tags = "Lista de Desejos", description = "API de gerenciamento de lista de desejos")
public class ListaDesejosResource {
	
	@Autowired
	private ListaDesejosService service;
	
	@GetMapping("/email/{email}")
	@ApiOperation(value = "Buscar Listas de desejos por email",  consumes = APPLICATION_JSON_VALUE, response=ListaDesejos.class, responseContainer="List", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ListaDesejos>> listarPorEmail(@PathVariable String email) {
		final List<ListaDesejos> listaDesejos = service.listarPorEmail(email);
		if(listaDesejos.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(listaDesejos);
	}
	
	@PostMapping("/")
	@ApiOperation(value = "Criação de Lista de Desejos",  consumes = APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> criarLista(@RequestBody ListaDesejosForm form, final HttpServletRequest request) {
		final ListaDesejos listaDesejos = service.criarListaDesejos(form.getNome(), form.getEmail(), form.getIdProduto());
		UriComponentsBuilder location = UriComponentsBuilder
				.fromPath(request.getRequestURL().append("/{id}").toString());
		URI uri = location.buildAndExpand(listaDesejos.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}

	@GetMapping("/{id}")
	public ResponseEntity<ListaDesejos> obterPorNomeEEmail(Long id){
		final Optional<ListaDesejos> listaDesejo = Optional.ofNullable(service.obterPorId(id));
		return listaDesejo.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
	}

	@PutMapping(path = "/{idListaDesejos}/produto")
	public ResponseEntity<Void> adicionarProduto(@PathVariable Long idListaDesejos, @RequestBody ProdutoListaDesejo produto) {
		service.adicionarProduto(idListaDesejos, produto);
		return ResponseEntity.ok().build();
	}

}
