package br.com.amazon.aoj.listadedesejos.resources.form;

import java.util.List;

import lombok.Data;

@Data
public class ListaDesejosForm {
	
	private String email;
	private String nome;
	private List<ProdutoListaDesejoForm> idProduto;
		
}
