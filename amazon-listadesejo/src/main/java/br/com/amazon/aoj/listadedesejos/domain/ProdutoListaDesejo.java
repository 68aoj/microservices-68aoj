package br.com.amazon.aoj.listadedesejos.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name="PRODUTO_LD")
public class ProdutoListaDesejo {
	
	@Id
	@Column(name="ID")
	private Long id;
	
	@Column(name="NOME")
	private String nome;
	
	@Column(name="CATEGORIA")
	private String categoria;
	
	@Column(name="FABRICANTE")
	private String fabricante;
}
