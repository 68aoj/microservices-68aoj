package br.com.amazon.aoj.listadedesejos.resources.form;

import lombok.Data;

@Data
public class ProdutoListaDesejoForm {
	
	private Long id;
	
	private String nome;
	
	private String categoria;
	
	private String fabricante;
}
