package br.com.amazon.aoj.listadedesejos.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.google.common.collect.Sets;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor(force=true, access=AccessLevel.PROTECTED)
@AllArgsConstructor
@Entity
@Table(name="LISTA_DESEJOS")
public class ListaDesejos {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private final Long id = null;
	private final String nome;
	private final String email;
	
	@Builder.Default
	@ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
		        name = "produto_lista_desejos", 
		        joinColumns = { @JoinColumn(name = "lista_desejos_id") }, 
		        inverseJoinColumns = { @JoinColumn(name = "produto_id") }
		    )
	private final Set<ProdutoListaDesejo> produtos = Sets.newHashSet();

	public void adicionarProduto(ProdutoListaDesejo produto) {
		this.produtos.add(produto);
	}
	
	public void removerProduto(Long produtoId) {
		produtos.removeIf(p -> p.getId().equals(produtoId));
	}
	
}
