::Inicia o Eureka Service
start cmd /k Call .\amazon-eurekasvr\eureka-run.bat
::Aguarda 20 segundos para iniciar os outros processos
TIMEOUT /T 20
::Inicia o Configuration Service
start cmd /k Call .\amazon-confsvr\config-run.bat
::Aguarda 20 segundos para iniciar os outros processos
TIMEOUT /T 15
::Inicia todos os outros serviços
start cmd /k Call .\amazon-frete\frete-run.bat
start cmd /k Call .\amazon-pedido\pedido-run.bat
start cmd /k Call .\amazon-produto\produto-run.bat
start cmd /k Call .\amazon-listadesejo\listadesejo-run.bat
start cmd /k Call .\amazon-suporte\suporte-run.bat

