package br.com.amazon.aoj.produto.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.amazon.aoj.produto.domain.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {


	List<Produto> findAllByCategoria(String categoria);

	List<Produto> findByNomeIn(String palavraChave);
	
	@Query("SELECT p FROM Produto p where p.nome LIKE CONCAT('%',:palavraChave,'%') OR p.categoria LIKE CONCAT('%',:palavraChave,'%')")
	Set<Produto> buscarPorPalavraChave(@Param("palavraChave") String palavraChave);
}
