package br.com.amazon.aoj.produto.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import br.com.amazon.aoj.produto.domain.Produto;
import br.com.amazon.aoj.produto.events.ProdutoEventSource;
import br.com.amazon.aoj.produto.repository.ProdutoRepository;


@Service
public class ProdutoService {
	
	@Autowired
	private ProdutoRepository repository;
	@Autowired 
	private ProdutoEventSource eventService;
	
	@Transactional
	public Produto salvar(final Produto produto) {
		final Produto produtoSaved = repository.save(produto);
		eventService.publishProdutoChange("SAVE", produtoSaved);
		return produtoSaved;
	}
	
	@Transactional
	public void deletar(final Long id) {
		buscar(id).ifPresent(produto ->{
			eventService.publishProdutoChange("DELETE", produto);
			repository.deleteById(id);
		});;
	} 
	
	@Transactional
	public Produto atualizar(final Produto produto) {
		eventService.publishProdutoChange("UPDATE", produto);
		return repository.save(produto);
	} 
	
	public Optional<Produto> buscar(final Long id) {
		return repository.findById(id);
	}
	
	public List<Produto> listar() {
		return repository.findAll();
	}
	
	public List<Produto> listarPorCategoria(String categoria) {
		return repository.findAllByCategoria(categoria);
	}
	
	public List<Produto> listarProdutosPor(final String nome, final String categoria, final String fabricante) {
		final Produto produto = Produto.builder()
				.categoria(categoria)
				.fabricante(fabricante)
				.nome(nome)
			.build();
		
		return repository.findAll(Example.of(produto, ExampleMatcher.matchingAny().withIgnoreCase()));

	}

}