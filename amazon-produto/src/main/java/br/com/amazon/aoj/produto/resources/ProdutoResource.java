package br.com.amazon.aoj.produto.resources;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.amazon.aoj.produto.domain.Produto;
import br.com.amazon.aoj.produto.domain.VisualizacaoProduto;
import br.com.amazon.aoj.produto.services.ProdutoService;
import br.com.amazon.aoj.produto.services.VisualizacaoProdutoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "produto")
@Api(tags = "Produtos", description = "API de Produtos")
public class ProdutoResource {
	
	@Autowired
	private ProdutoService service;
	@Autowired
	private VisualizacaoProdutoService visualizacaoProdutoService;  
	
	
	@GetMapping("/")
	@ApiOperation(value = "Buscar Listas de Produtos",  consumes = APPLICATION_JSON_VALUE, response=Produto.class, responseContainer="List", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Produto>> listar(){
		final List<Produto> produtos = service.listar();
		if(produtos.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(produtos);
	}
	
	@GetMapping("/{id}")
	@ApiOperation(value = "Buscar um produto por id", response=Produto.class, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<Produto> obter(@PathVariable Long id){
		final Optional<Produto> produto = service.buscar(id);
		produto.ifPresent(visualizacaoProdutoService::adicionarVisualizacao);
		return produto.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
	}

	@GetMapping(path="/pesquisar", params= {"nome", "categoria", "fabricante"})
	@ApiOperation(value = "Buscar Listas de Produtos por nome, categoria ou fabricante",  consumes = APPLICATION_JSON_VALUE, response=Produto.class, responseContainer="List", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Produto>> buscar(
			@RequestParam(name="nome", required=false, defaultValue="") String nome,
			@RequestParam(name="categoria", required=false, defaultValue="") String categoria,
			@RequestParam(name="fabricante", required=false, defaultValue="") String fabricante){
		final List<Produto> produtos = service.listarProdutosPor(nome, categoria, fabricante);
		if(produtos.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(produtos);
	}

	@GetMapping("/maisvisualizados/{categoria}")
	@ApiOperation(value = "Buscar listas de produtos mais visualizador por categoria",  response=VisualizacaoProduto.class, responseContainer="List", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<List<VisualizacaoProduto>> produtosMaisVistosPorCategoria(@PathVariable("categoria") String categoria) {
		final List<VisualizacaoProduto> produtos = visualizacaoProdutoService.produtosMaisVistosPorCategoria(categoria);;
		if(produtos.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(produtos);
	}

	@PostMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Serviço de criação de produto",  consumes = APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> incluir(@RequestBody Produto form, final HttpServletRequest request) {
		final Produto produto = service.salvar(form);
		UriComponentsBuilder location = UriComponentsBuilder
				.fromPath(request.getRequestURL().append("/{id}").toString());
		URI uri = location.buildAndExpand(produto.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}
	
	@PutMapping(path="/{id}",  consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Serviço de atualização de produto",  consumes = APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> atualizar(@PathVariable("id") Long id, @RequestBody Produto produto) {
		service.atualizar(produto);
		return ResponseEntity.ok().build();
	}
	
	@DeleteMapping(path = "/{id}")
	@ApiOperation(value = "Serviço de remoção de produto",  consumes = APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> deletar(@PathVariable Long id ) {
		service.deletar(id);
		return ResponseEntity.noContent().build();
	}

}