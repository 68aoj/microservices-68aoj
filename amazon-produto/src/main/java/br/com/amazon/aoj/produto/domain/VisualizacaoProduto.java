package br.com.amazon.aoj.produto.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="VISUALIZACAO_PRODUTO")
public class VisualizacaoProduto {

	@Id
	private Long idProduto;
	private String categoria;
	private long quantidade;
	
	
	
	public VisualizacaoProduto() {

	}

	public VisualizacaoProduto(Long idProduto, String categoria) {
		super();
		this.idProduto = idProduto;
		this.categoria = categoria;
		this.quantidade = 1;
	}
	
	public void visualizado() {
		this.quantidade += 1;
	}

	public Long getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(Long idProduto) {
		this.idProduto = idProduto;
	}

	public long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(long quantidade) {
		this.quantidade = quantidade;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	
	public String quantidadeVisualizacoes() {
		return "Visualizado " + quantidade + " vezes ";
	}
	
	
}
