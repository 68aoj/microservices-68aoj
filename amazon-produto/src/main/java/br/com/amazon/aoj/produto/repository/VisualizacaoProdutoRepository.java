package br.com.amazon.aoj.produto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.amazon.aoj.produto.domain.VisualizacaoProduto;


public interface VisualizacaoProdutoRepository extends JpaRepository<VisualizacaoProduto, Long>{
	
	List<VisualizacaoProduto> findByCategoria(String categoria);

	
}
