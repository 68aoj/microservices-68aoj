package br.com.amazon.aoj.produto.events;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import br.com.amazon.aoj.produto.domain.Produto;
import br.com.amazon.aoj.produto.events.models.ProdutoChangeModel;

@Component
public class ProdutoEventSource {

	private static final Logger logger = LoggerFactory.getLogger(ProdutoEventSource.class);

	@Autowired
	private Source source;

	@Async
	public void publishProdutoChange(String action, Produto produto) {
		logger.debug("Sending Kafka message {} for Produto Id: {}", action, produto.getId());
		ProdutoChangeModel change = new ProdutoChangeModel(ProdutoChangeModel.class.getTypeName(),
				action, produto.getId(), produto.getNome(),produto.getCategoria(), produto.getFabricante());
		source.output().send(MessageBuilder.withPayload(change).build());
	}

}
