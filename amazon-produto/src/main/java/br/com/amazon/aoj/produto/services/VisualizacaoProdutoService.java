package br.com.amazon.aoj.produto.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;

import br.com.amazon.aoj.produto.domain.Produto;
import br.com.amazon.aoj.produto.domain.VisualizacaoProduto;
import br.com.amazon.aoj.produto.repository.VisualizacaoProdutoRepository;

@Service
public class VisualizacaoProdutoService {

	@Autowired
	private VisualizacaoProdutoRepository repository;
	
	@Transactional(rollbackFor = Exception.class)
	public void adicionarVisualizacao(Produto produto) {
		if (!repository.findById(produto.getId()).isPresent()) {
			repository.save(new VisualizacaoProduto(produto.getId(), produto.getCategoria()));
			return;
		} 
		repository.findById(produto.getId()).get().visualizado();
	}
	
	public List<VisualizacaoProduto> produtosMaisVistosPorCategoria(String categoria) {
		return repository.findByCategoria(categoria).stream()
							.sorted(Comparator.comparingLong(VisualizacaoProduto::getQuantidade).reversed())
							.collect(Collectors.toList());
	}
}
