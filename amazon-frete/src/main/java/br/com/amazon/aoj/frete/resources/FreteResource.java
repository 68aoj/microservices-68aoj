package br.com.amazon.aoj.frete.resources;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.amazon.aoj.frete.service.FreteService;
import io.swagger.annotations.Api;


@RestController
@RequestMapping("frete")
@Api(tags = "Frete", description = "Calcula o Frete dos Pedidos realizados")
public class FreteResource {
	
	@Autowired
	private FreteService service;
	
	@GetMapping("/calcular/{cep}")
	public BigDecimal calcular(@PathVariable("cep") String cep) {
		return service.calcular(cep);
	}

}
