package br.com.amazon.aoj.pedido.domain.form;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;

@Data
public class ItemPedidoForm implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long idProduto;
	private String nome;
	private String categoria;
	private String fabricante;
	private int quantidade;
	private BigDecimal preco;
	
}
	