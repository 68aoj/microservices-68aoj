package br.com.amazon.aoj.pedido.events.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor(force=true)
public class ProdutoChangeModel {
	private final String type;
	private final String action;
	private final Long produtoId;
	private final String nome;
	private final String categoria;
	private final String fabricante;
}
