package br.com.amazon.aoj.pedido.events;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface CustomChannels {
	
	@Input("inboundProdutoChanges")
	SubscribableChannel inboundProdutoChanges();
}