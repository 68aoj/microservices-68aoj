package br.com.amazon.aoj.pedido.domain;

import java.math.BigDecimal;

import javax.persistence.Embeddable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor(access=AccessLevel.PROTECTED, force = true)
@Builder(toBuilder=true)
@Embeddable
public class ItemPedido {

	private Long id;
	private Long produtoId;
	private String nome;
	private String categoria;
	private String fabricante;
	private int quantidade;
	private BigDecimal preco;
	
}
