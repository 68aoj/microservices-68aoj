package br.com.amazon.aoj.pedido.services;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.amazon.aoj.pedido.domain.ItemPedido;
import br.com.amazon.aoj.pedido.domain.Pedido;
import br.com.amazon.aoj.pedido.domain.form.PedidoForm;
import br.com.amazon.aoj.pedido.repository.PedidoRepository;

@Service
public class PedidoService {

	@Autowired
	private PedidoRepository pedidoRepository;
	
	@Autowired
	private FreteService freteService;
	
	public List<Pedido> listar() {
		return pedidoRepository.findAll();
	}

	public Pedido obter(Long id) {
		return pedidoRepository.findById(id).get();
	}

	@Transactional(rollbackFor = Exception.class)
	public Pedido incluir(PedidoForm pedidoForm) {
		
		final BigDecimal valorFrete = freteService.obterValorFrete(pedidoForm.getCep());
		final List<ItemPedido> itens = pedidoForm.getItens().stream()
			.map(item -> 
					ItemPedido.builder()
						.categoria(item.getCategoria())
						.produtoId(item.getIdProduto())
						.fabricante(item.getFabricante())
						.id(item.getIdProduto())
						.nome(item.getNome())
					.build()
				).collect(Collectors.toList());
		
		final Pedido pedido = Pedido.builder()
				.cliente(pedidoForm.getCliente())
				.data(pedidoForm.getData())
				.email(pedidoForm.getEmail())
				.valorFrete(valorFrete)
			.itens(itens).build();
		
		return pedidoRepository.save(pedido);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void alterarNomeDoProdutoNosPedidos(Long produtoId, String nome) {
		List<Pedido> pedidos = pedidoRepository.findAll();
		pedidos.stream()
				.flatMap(p -> p.getItens().stream())
				.filter(item -> item.getProdutoId().equals(produtoId))
				.forEach(item -> item.setNome(nome));

		pedidoRepository.saveAll(pedidos);
		
	}
	

}
