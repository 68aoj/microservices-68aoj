package br.com.amazon.aoj.pedido.resources;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.amazon.aoj.pedido.domain.Pedido;
import br.com.amazon.aoj.pedido.domain.form.PedidoForm;
import br.com.amazon.aoj.pedido.services.PedidoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "pedidos")
@Api(value = "Pedidos", description = "API de Pedidos")
public class PedidoResource {
	
	@Autowired
	private PedidoService service;
	
	@GetMapping("/")
	@ApiOperation(value = "Buscar Listas de Pedidos",  consumes = APPLICATION_JSON_VALUE, response=Pedido.class, responseContainer="List", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Pedido>> listar(){
		final List<Pedido> pedidos = service.listar();
		if(pedidos.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(pedidos);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Busca um pedido por id",  consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE, response=Pedido.class)
	public ResponseEntity<Pedido> obter(@PathVariable("id") Long id){
		final Optional<Pedido> pedido = Optional.ofNullable(service.obter(id));
		return pedido.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Criação de pedido",  consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE, response=Pedido.class)
	public ResponseEntity<Void> incluir(@RequestBody PedidoForm form, final HttpServletRequest request) {
		final Pedido pedido = service.incluir(form);
		UriComponentsBuilder location = UriComponentsBuilder
				.fromPath(request.getRequestURL().append("/{id}").toString());
		URI uri = location.buildAndExpand(pedido.getId()).toUri();
		return ResponseEntity.created(uri).build();
		
	}
	

}
