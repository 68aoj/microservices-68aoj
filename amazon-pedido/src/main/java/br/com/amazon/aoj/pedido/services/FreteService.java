package br.com.amazon.aoj.pedido.services;

import java.math.BigDecimal;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class FreteService {

	@Autowired
	private DiscoveryClient discoveryClient;
	
	public BigDecimal obterValorFrete(Long cep) {

		  List<ServiceInstance> instances = discoveryClient.getInstances("amazonfrete");
		  URI uri = instances.get(0).getUri();
  
		  Map<String, Long> uriVariables = new HashMap<>();
		    uriVariables.put("cep", cep);               
		    ResponseEntity<BigDecimal> responseEntity = new RestTemplate().getForEntity(
		    		uri + "/frete/calcular/{cep}", 
		    		BigDecimal.class, 
		            uriVariables );     
		    return responseEntity.getBody(); 
	}
	
}
