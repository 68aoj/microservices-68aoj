package br.com.amazon.aoj.pedido.events.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Service;

import br.com.amazon.aoj.pedido.events.CustomChannels;
import br.com.amazon.aoj.pedido.events.models.ProdutoChangeModel;
import br.com.amazon.aoj.pedido.services.PedidoService;

@EnableBinding(CustomChannels.class)
public class ProdutoChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(ProdutoChangeHandler.class);
	
	@Autowired
	private PedidoService pedidoService;

	@StreamListener("inboundProdutoChanges")
	public void loggerSink(ProdutoChangeModel produtoChange) {
		logger.debug("Received a message of type " + produtoChange.getType());
		switch (produtoChange.getAction()) {
		
		case "UPDATE":
			logger.debug("Received a UPDATE event from the produto service for produto id {}",
					produtoChange.getProdutoId());
			pedidoService.alterarNomeDoProdutoNosPedidos(produtoChange.getProdutoId(), produtoChange.getNome());
			break;
		default:
			logger.error("Received an UNKNOWN event from the produto service of type {}", produtoChange.getType());
			break;
		}
	}

}
