package br.com.amazon.aoj.pedido.infra;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	@Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)
	          .groupName("amazon-pedido").apiInfo(apiInfo()).select()
	          .apis(RequestHandlerSelectors.basePackage("br.com.amazon.aoj.pedido"))              
	          .paths(PathSelectors.any())
          .build();                                           
    }
	
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Serviços de Amazon Pedido")
					.description("Serviços que mostram dados dos pedidos realizados")
					.version("1.0.0")
				.build();
	}
}
