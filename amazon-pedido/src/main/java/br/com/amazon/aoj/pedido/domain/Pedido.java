package br.com.amazon.aoj.pedido.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access=AccessLevel.PROTECTED, force = true)
@Entity
@Table(name = "pedido")
public class Pedido {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_PEDIDO")
	private final Long id = null;
	private final String cliente;
	private final String email;
	
	@Builder.Default
	@Enumerated(EnumType.STRING)
	private final StatusPedido status = StatusPedido.ANALISANDO;
	
	@Temporal(TemporalType.TIME)
	private final Date data;
	
	@ElementCollection
	@CollectionTable(name = "ITEM_PEDIDO", joinColumns = @JoinColumn(name = "ID_PEDIDO"))
	private final List<ItemPedido> itens;
	
	@Column(name="VALOR_FRETE")
	private BigDecimal valorFrete;

	enum StatusPedido {
		ANALISANDO, APROVADO, EM_SEPARACAO, SEPARADO, PRONTO_PARA_ENVIO, ENVIADO;
	}

}
