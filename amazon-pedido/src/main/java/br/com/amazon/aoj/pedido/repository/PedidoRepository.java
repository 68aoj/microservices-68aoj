package br.com.amazon.aoj.pedido.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.amazon.aoj.pedido.domain.Pedido;

public interface PedidoRepository extends JpaRepository<Pedido, Long>{
	
}
