package br.com.amazon.aoj.pedido.domain.form;

import java.util.Date;
import java.util.List;

import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PedidoForm {
	
	private String cliente;
	private String email;
	private Date data;
	private Long cep;
	private List<ItemPedidoForm> itens = Lists.newArrayList();
	
	

}
